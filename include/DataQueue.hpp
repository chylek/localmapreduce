#ifndef DATAQUEUE_HPP
#define DATAQUEUE_HPP

#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <utility>
#include <cinttypes>

#include <RawData.hpp>

namespace lmr {
    class DataQueue {
        private:
            std::queue<RawData> data;
            std::mutex mux;
            std::condition_variable cv;
            std::atomic<uint64_t> q_size;
            bool eot = false;

        public:
            DataQueue ();
            DataQueue (const DataQueue &queue);
            DataQueue (DataQueue &&queue);
            void push (RawData data);
            bool front (RawData &rawdata);
            bool back (RawData &rawdata);
            bool pop (RawData &rawdata);
            bool getEOT ();
            void setEOT (bool eot);
            bool empty ();
            void clear ();
            uint64_t size ();
            DataQueue& operator= (const DataQueue &queue);
            DataQueue& operator= (DataQueue &&queue);
    };
}

#endif
