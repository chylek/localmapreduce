#ifndef RAWDATA_HPP
#define RAWDATA_HPP

#include <memory>
#include <mutex>
#include <iostream>
#include <atomic>
#include <thread>
#include <utility>

namespace lmr {
    class RawData;
    typedef bool(*RawDataCmp)(const RawData&, const RawData&);

    class DataConcept {
        public:
            virtual ~DataConcept () {}
    };

    template<typename T> class DataModel: public DataConcept {
        public:
            T data;

            DataModel (const T &t): data(t) {}
            virtual ~DataModel () {}
    };

    class RawData {
        private:
            std::shared_ptr<DataConcept> data;
            
        public:
            RawDataCmp cmp;

            RawData (): data(nullptr) {}
            
            template<typename T> RawData (const T &rd, RawDataCmp cmp_cb = RawData::defaultCmp<T>): data(nullptr) {
                this->data = std::make_shared<DataModel<T>>(rd);
                this->cmp = cmp_cb;
            }

            RawData (const RawData &raw_data): data(raw_data.data) {
                this->cmp = raw_data.cmp;
            }

            RawData (RawData &&raw_data): data(std::move(raw_data.data)) {
                this->cmp = raw_data.cmp;
            }

            ~RawData () {
                this->data.reset();
            }

            template<typename T> T& get () const {
                return ((DataModel<T>*)(this->data.get()))->data;
            }

            template<typename T> static bool defaultCmp (const RawData &raw_data1, const RawData &raw_data2) {
                return raw_data1.get<T>() < raw_data2.get<T>();
            }

            RawData& operator= (const RawData &raw_data) {
                this->data = raw_data.data;
                this->cmp = raw_data.cmp;
                return *this;
            }

            RawData& operator= (RawData &&raw_data) {
                this->data = std::move(raw_data.data);
                raw_data.data = nullptr;
                this->cmp = raw_data.cmp;
                return *this;
            }

            bool operator< (const RawData &raw_data) const {
                return this->cmp(*this, raw_data);
            }

            bool operator<= (const RawData &raw_data) const {
                return !this->cmp(raw_data, *this);
            }

            bool operator> (const RawData &raw_data) const {
                return this->cmp(raw_data, *this);
            }

            bool operator>= (const RawData &raw_data) const {
                return !this->cmp(*this, raw_data);
            }

            bool operator== (const RawData &raw_data) const {
                return !this->cmp(*this, raw_data) && !this->cmp(raw_data, *this);
            }   
        };
}

#endif
