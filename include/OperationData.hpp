#ifndef LMR_OPERATION_DATA_HPP
#define LMR_OPERATION_DATA_HPP

#include <string>
#include <map>
#include <vector>

#include <RawData.hpp>

namespace lmr {
    using mapCallback = RawData (*)(const RawData&);
    using reduceCallback = RawData (*)(const RawData&, const RawData&, const RawData&);
    using filterCallback = bool (*)(const RawData&);
    using joinCallback = RawData (*)(const RawData&, const RawData&);

    using pair = std::pair<RawData, RawData>;
    using vector = std::vector<RawData>;
    using map = std::map<RawData, RawData>;

    enum operationType {
        LoadFromFile,
        Map,
        FlatMap,
        Reduce,
        Filter,
        Join,
        Collect,
        None
    };

    struct OperationData {
        operationType operation_type;
        std::vector<std::string> load_arg;
        std::map<RawData, RawData> *join_arg;
        mapCallback map_cb;
        reduceCallback reduce_cb;
        filterCallback filter_cb;

        OperationData (operationType operation_type = operationType::None); /* allow to use empty constructor and to create Collect operation */
        OperationData (std::string load_arg);
        OperationData (std::vector<std::string> &load_arg);
        OperationData (std::map<RawData, RawData>* join_arg);
        OperationData (mapCallback map_cb, bool is_flat_map = false);
        OperationData (reduceCallback reduce_cb);
        OperationData (filterCallback filter_cb);
        OperationData (const OperationData &op_data);
        OperationData (OperationData &&op_data);
        ~OperationData ();
        void nullify ();
        void reset ();
        OperationData& operator= (const OperationData &record);
        OperationData& operator= (OperationData &&record);
    };
}

#endif
