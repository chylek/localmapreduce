#ifndef LOCALMAPREDUCE_HPP
#define LOCALMAPREDUCE_HPP

#include <map>
#include <string>
#include <list>
#include <fstream>
#include <queue>
#include <thread>
#include <mutex>
#include <atomic>

#include <RawData.hpp>
#include <OperationData.hpp>
#include <DataQueue.hpp>

#define LMR_EPSILON 0.001f

namespace lmr {
    class Mapper {
        public:
            RawData key, value;

            virtual void exec ();
            void setArgs (const RawData &key, const RawData &value); // to simplify usage
    };

    class Reducer {
        public:
            RawData value;

            virtual void exec ();
    };

    enum elementType {
        Pair,
        Data
    };

    class Collection {
        protected:
            bool show_warnings = true, recursive_flat_map = true;
            unsigned int workers_num;
            elementType element_type = elementType::Data;
            std::list<OperationData> operation_queue;
            std::map<RawData, RawData> data1, data2, *curr_data, *last_data;
            std::mutex curr_mux, last_mux;

            void reset ();
            static void inputThreadCaller (Collection *collection, DataQueue *input_queue, const operationType *input_source, const std::string *file_name);
            static void workerThreadCaller (Collection *collection, DataQueue *input_queue, const std::list<OperationData> *local_operation_queue, std::atomic<uint64_t> *map_num_index);
            void inputThread (DataQueue *input_queue, const operationType *input_source, const std::string *file_name);
            void workerThread (DataQueue *input_queue, const std::list<OperationData> *local_operation_queue, std::atomic<uint64_t> *map_num_index);
            void exec ();

        public:
            Collection (unsigned int workers_num = 4); // 4 is probably the most common number of cores
            Collection (const Collection &collection);
            Collection (Collection &&collection);
            ~Collection ();

            Collection& loadFromFile (std::string file);
            Collection& loadFromVector (std::vector<lmr::RawData> &v);
            Collection& loadFromMap (std::map<lmr::RawData, lmr::RawData> &m);
            Collection& map (mapCallback callback);
            Collection& flatMap (mapCallback callback);
            Collection& reduce (reduceCallback callback);
            Collection& filter (filterCallback callback);
            std::map<RawData, RawData>* collect ();
            Collection& join (Collection collection);

            unsigned long int size ();
            void setWorkersNumber (unsigned int workers_num);
            void setWarnings (bool warnings);
            void setRecursiveFlatMap (bool recursive);

            Collection operator= (const Collection &collection);
            Collection operator= (Collection &&collection);
        };
}

#endif
