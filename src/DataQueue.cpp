#include <DataQueue.hpp>

namespace lmr {
    DataQueue::DataQueue () {
        this->q_size = 0;
    }

    DataQueue::DataQueue (const DataQueue &queue) {
        this->eot = queue.eot;
        this->q_size = queue.q_size.load();
        this->data = std::queue<RawData>(queue.data);
    }

    DataQueue::DataQueue (DataQueue &&queue) {
        this->eot = queue.eot;
        queue.eot = false;
        this->q_size = queue.q_size.load();
        queue.q_size = 0;
        this->data = std::move(queue.data);
    }

    void DataQueue::push (RawData data) {
        std::unique_lock<std::mutex> l(this->mux);
        this->data.push(data);
        this->q_size++;
        this->cv.notify_all();
    }

    bool DataQueue::front (RawData &rawdata) {
        bool success = true;
        std::unique_lock<std::mutex> l(this->mux);

        if(!this->eot) {
            this->cv.wait(l, [this]() {
                return !this->data.empty() || this->eot;
            });
        }
        if(!this->data.empty()) {
            rawdata = this->data.front();
        } else {
            success = false;
        }
        return success;
    }

    bool DataQueue::back (RawData &rawdata) {
        bool success = true;
        std::unique_lock<std::mutex> l(this->mux);

        if(!this->eot) {
            this->cv.wait(l, [this]() {
                return !this->data.empty() || this->eot;
            });
        }
        if(!this->data.empty()) {
            rawdata = this->data.back();
        } else {
            success = false;
        }
        return success;
    }

    bool DataQueue::pop (RawData &rawdata) {
        bool success = true;
        std::unique_lock<std::mutex> l(this->mux);

        if(!this->eot) {
            this->cv.wait(l, [this]() {
                return !this->data.empty() || this->eot;
            });
        }
        if(!this->data.empty()) {
            rawdata = this->data.front();
            this->data.pop();
            this->q_size--;
        } else {
            success = false;
        }
        return success;
    }

    bool DataQueue::getEOT () {
        std::unique_lock<std::mutex> l(this->mux);
        return this->eot;
    }

    void DataQueue::setEOT (bool eot) {
        std::unique_lock<std::mutex> l(this->mux);

        this->eot = eot;
        this->cv.notify_all();
    }

    bool DataQueue::empty () {
        std::unique_lock<std::mutex> l(this->mux);
        return this->q_size;
    }

    void DataQueue::clear () {
        std::unique_lock<std::mutex> l(this->mux);
        this->data = std::queue<RawData>();
        this->q_size = 0;
    }

    uint64_t DataQueue::size () {
        std::unique_lock<std::mutex> l(this->mux);
        return this->q_size;
    }

    DataQueue& DataQueue::operator= (const DataQueue &queue) {
        this->eot = queue.eot;
        this->q_size = queue.q_size.load();
        this->data = std::queue<RawData>(queue.data);
        return *this;
    }

    DataQueue& DataQueue::operator= (DataQueue &&queue) {
        this->eot = queue.eot;
        queue.eot = false;
        this->q_size = queue.q_size.load();
        queue.q_size = 0;
        this->data = std::move(queue.data);
        return *this;
    }
}

