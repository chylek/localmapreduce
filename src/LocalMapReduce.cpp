#include <cstring>
#include <string>
#include <map>
#include <list>
#include <unistd.h>

#include <LocalMapReduce.hpp>

#define MEGABYTE (1024ULL * 1024ULL)

// TODO:
// - change implementation of join to make it in thread maybe(?) - to dicuss
// - check if it's possible to protect by mutex only the very field instead of the whole map - code to do so is commented-out

namespace lmr {
    void Mapper::setArgs (const RawData &key, const RawData &data) {
        this->key = key;
        this->value = value;
    }

    void Collection::reset () {
        this->show_warnings = true;
        this->recursive_flat_map = true;
        this->workers_num = 4;
        this->element_type = elementType::Data;
        this->operation_queue.clear();
        this->data1.clear();
        this->data2.clear();
        this->curr_data = &this->data1;
        this->last_data = &this->data2;
    }

    void Collection::inputThreadCaller (Collection *collection, DataQueue *input_queue, const operationType *input_source, const std::string *file_name) {
        collection->inputThread(input_queue, input_source, file_name);
    }

    void Collection::workerThreadCaller (Collection *collection, DataQueue *input_queue, const std::list<OperationData> *local_operation_queue, std::atomic<uint64_t> *map_num_index) {
        collection->workerThread(input_queue, local_operation_queue, map_num_index);
    }

    void Collection::inputThread (DataQueue *input_queue, const operationType *input_source, const std::string *file_name) {
        if(*input_source == operationType::LoadFromFile) { // read data from the file
            std::ifstream file(*file_name);
            std::string tmp_str;
            
            this->element_type = elementType::Data;
            while(getline(file, tmp_str)) {
                if(tmp_str.size() > MEGABYTE && this->show_warnings) {
                    std::cout << "WARNING: Long lines on input can decreese the efficiency" << std::endl;
                }
                input_queue->push(tmp_str);
            }
          
            file.close();
        } else { // read data from internal data container
            this->curr_mux.lock();
            this->last_mux.lock();
            std::swap(this->last_data, this->curr_data);
            this->curr_data->clear();
            this->last_mux.unlock();
            this->curr_mux.unlock();

            this->last_mux.lock();
            if(this->element_type == elementType::Pair) {
                for(std::pair<RawData, RawData> p: *this->last_data) {
                    input_queue->push(p);
                }
            } else {
                for(std::pair<RawData, RawData> p: *this->last_data) {
                    input_queue->push(p.second);
                }
            }
            this->last_mux.unlock();
        }
    }

    void Collection::workerThread (DataQueue *input_queue, const std::list<OperationData> *local_operation_queue, std::atomic<uint64_t> *map_num_index) {
        bool repeat = true, success, for_guard;
        RawData element(NULL);

        while(repeat) {
            success = input_queue->pop(element);

            if(!success) { // end of input
                repeat = false;
                continue;
            } else {
                for_guard = true;
                for(const OperationData &operation_record: *local_operation_queue){
                    if(!for_guard) {
                        break;
                    }
                    switch(operation_record.operation_type) {
                        case Map: {
                            element = operation_record.map_cb(element);
                            this->element_type = elementType::Pair; // just needed in collect - only correct usage of map at the end of queue is returning pair
                            break;
                        }
                        case FlatMap: {
                            RawData rd_v = operation_record.map_cb(element); // makes it possible to use reference instead of copying the whole vector
                            std::vector<RawData> &v = rd_v.get<std::vector<RawData>>();

                            if(this->recursive_flat_map) {
                                DataQueue new_queue;
                                std::list<OperationData> new_list = std::list<OperationData>(*local_operation_queue);

                                // do not do the operations twice - drop the already done ones
                                while(new_list.front().operation_type != operationType::FlatMap) { // no need to check size, FlatMap has to be there
                                    new_list.pop_front();
                                }
                                new_list.pop_front(); // drop the FlatMap
                                for(unsigned int k=0; k<v.size(); k++) {
                                    new_queue.push(v[k]);
                                }
                                new_queue.setEOT(true); // it's not event parallel
                                this->workerThread(&new_queue, &new_list, map_num_index);
                            } else {
                                this->curr_mux.lock();
                                for(unsigned int k=0; k<v.size(); k++) {
                                    (*this->curr_data)[++(*map_num_index)] = v[k];
                                }
                                this->curr_mux.unlock();
                            }
                            for_guard = false; // break the for loop
                            this->element_type = elementType::Data;
                            break;
                        }
                        case Reduce: {
                            RawData key = element.get<std::pair<RawData, RawData>>().first;
                            RawData value = element.get<std::pair<RawData, RawData>>().second;

                            this->curr_mux.lock();
                            if(this->curr_data->count(key)) {
                                RawData curr_val = (*this->curr_data)[key];
                                (*this->curr_data)[key] = operation_record.reduce_cb(key, value, curr_val); // actual reduce step execution
                            } else {
                                (*this->curr_data)[key] = value;
                            }
                            this->curr_mux.unlock();

                            for_guard = false; // break the for loop
                            this->element_type = elementType::Pair;
                            break;
                        }
                        case Filter:
                            if(!operation_record.filter_cb(element)) {
                                for_guard = false; // break the for loop - just drop this entry
                            }
                            break;
                        case Join: {
                            std::pair<RawData, RawData> p = element.get<std::pair<RawData, RawData>>();
                            std::map<RawData, RawData> *join_arg = operation_record.join_arg;

                            // no need to protect join_arg as this is the only reference to the object
                            if(join_arg->count(p.first) > 0) {
                                this->curr_mux.lock();
                                (*this->curr_data)[p.first] = RawData(std::pair<RawData, RawData>(p.second, (*join_arg)[p.first]));
                                this->curr_mux.unlock();
                            }

                            for_guard = false; // break the for loop
                            this->element_type = elementType::Pair;
                            break;
                        }
                        case LoadFromFile:
                            std::cerr << "ERROR: Load from file should not be on the list of operations" << std::endl;
                            break;
                        case Collect:
                            this->curr_mux.lock();
                            if(this->element_type == elementType::Data) {
                                (*this->curr_data)[++(*map_num_index)] = element;
                            } else {
                                std::pair<RawData, RawData> p = element.get<std::pair<RawData, RawData>> ();

                                (*this->curr_data)[p.first] = p.second;
                            }
                            this->curr_mux.unlock();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }


    void Collection::exec () {
        DataQueue input_queue;
        operationType input_source;
        std::atomic<uint64_t> map_num_index(0);
        std::vector<std::string> file_names;
        std::vector<std::string>::size_type input_thread_number;
        std::thread *input_thread, *worker_thread;

        if(operation_queue.size() == 0) {
            return;
        }
        if(this->operation_queue.back().operation_type != Collect
                && (this->recursive_flat_map || operation_queue.back().operation_type != FlatMap)
                && operation_queue.back().operation_type != Reduce) {
            this->operation_queue.push_back(OperationData(operationType::Collect));
        }

        if(operation_queue.size() > 0 && operation_queue.front().operation_type == LoadFromFile) {
            input_source = LoadFromFile;
            file_names = operation_queue.front().load_arg;
            operation_queue.pop_front();
        } else {
            input_source = None;
        }

        // pool of input threads
        input_thread_number = file_names.size();
        if(input_thread_number == 0) {
            input_thread_number = 1;
            file_names.push_back("");
        }
        input_thread = new std::thread[input_thread_number];
        for(unsigned int i=0; i<input_thread_number; i++) {
            input_thread[i] = std::thread(inputThreadCaller, this, &input_queue, &input_source, &file_names[i]);
        }

        // pool of workers
        worker_thread = new std::thread[this->workers_num];
        for(unsigned int i=0; i<this->workers_num; i++) {
            worker_thread[i] = std::thread(workerThreadCaller, this, &input_queue, &operation_queue, &map_num_index); // start threads
        }

        // wait for input threads
        for(unsigned int i=0; i<input_thread_number; i++) {
            input_thread[i].join();
        }
        input_queue.setEOT(true);

        for(unsigned int i=0; i<this->workers_num; i++) {
            worker_thread[i].join();
        }
        
        delete [] input_thread;
        delete [] worker_thread;
        
        while(operation_queue.size() > 0 &&
                operation_queue.front().operation_type != Reduce &&
                (this->recursive_flat_map || operation_queue.front().operation_type != FlatMap) &&
                operation_queue.front().operation_type != Join) {
            operation_queue.pop_front();
        }
        if(operation_queue.size() > 0) {
            operation_queue.pop_front();
        }
        if(operation_queue.size() > 0) {
            exec();
        }
    }

    Collection::Collection (unsigned int workers_num) {
        this->workers_num = workers_num;
        this->curr_data = &data1;
        this->last_data = &data2;
    }

    Collection::Collection (const Collection &collection) {
        this->workers_num = collection.workers_num;
        this->operation_queue = std::list<OperationData>(collection.operation_queue);
        this->data1 = std::map<RawData, RawData>(collection.data1);
        this->data2 = std::map<RawData, RawData>(collection.data2);
        if(collection.curr_data == &collection.data1) {
            this->curr_data = &this->data1;
            this->last_data = &this->data2;
        } else {
            this->curr_data = &this->data2;
            this->last_data = &this->data1;
        }
        this->element_type = collection.element_type;
    }

    Collection::Collection (Collection &&collection) {
        this->workers_num = collection.workers_num;
        this->operation_queue = std::move(collection.operation_queue);
        this->data1 = std::move(collection.data1);
        this->data2 = std::move(collection.data2);
        if(collection.curr_data == &collection.data1) {
            this->curr_data = &this->data1;
            this->last_data = &this->data2;
        } else {
            this->curr_data = &this->data2;
            this->last_data = &this->data1;
        }
        this->element_type = collection.element_type;
        collection.reset();
    }

    Collection::~Collection () {}

    Collection& Collection::loadFromFile (std::string file_path) {
        /* we don't need any operations before loading the data */
        if(this->operation_queue.size() > 0 && this->operation_queue.back().operation_type != operationType::LoadFromFile) {
            this->operation_queue = std::list<OperationData>(); /* nice way to clean the list */
        }
        if(this->operation_queue.size() > 0) {
            this->operation_queue.back().load_arg.push_back(file_path);
        } else {
            this->operation_queue.push_back(OperationData(file_path));
        }
        return *this;
    }

    Collection& Collection::loadFromVector (std::vector<RawData> &v) {
        unsigned int size = this->curr_data->size();
        for(unsigned int k=0; k<v.size(); k++) {
            this->curr_mux.lock();
            (*this->curr_data)[size+k] = v[k];
            this->curr_mux.unlock();
        }
        this->element_type = elementType::Data;
        return *this;
    }

    Collection& Collection::loadFromMap(std::map<RawData, RawData> &m) {
        for(std::pair<RawData, RawData> p: m) {
            this->curr_mux.lock();
            (*this->curr_data)[p.first] = p.second;
            this->curr_mux.unlock();
        }
        this->element_type = elementType::Pair;
        return *this;
    }

    Collection& Collection::map (mapCallback callback) {
        this->operation_queue.push_back(OperationData(callback));
        return *this;
    }

    Collection& Collection::flatMap (mapCallback callback) {
        this->operation_queue.push_back(OperationData(callback, true));
        return *this;
    }

    Collection& Collection::reduce (reduceCallback callback) {
        this->operation_queue.push_back(OperationData(callback));
        return *this;
    }

    Collection& Collection::filter (filterCallback callback) {
        this->operation_queue.push_back(OperationData(callback));
        return *this;
    }

    std::map<RawData, RawData>* Collection::collect () {
        std::map<RawData, RawData> *data = new std::map<RawData, RawData>();

        //this->operation_queue.push_back(OperationData(operationType::Collect));
        //doesnt change anything just makes exec to do one more step
        this->exec();
        this->curr_mux.lock();
        for(std::pair<RawData, RawData> p: *this->curr_data) {
            (*data)[p.first] = p.second;
        }
        this->curr_mux.unlock();
        return data;
    }

    Collection& Collection::join (Collection collection) {
        this->operation_queue.push_back(OperationData(collection.collect())); // LMR uses copy of user's collection
        return *this;
    }

    unsigned long int Collection::size () {
        return this->curr_data->size();
    }

    void Collection::setWorkersNumber (unsigned int workers_num) {
        if(workers_num) { // do not allow 0
            this->workers_num = workers_num;
        }
    }

    void Collection::setWarnings (bool warnings) {
        this->show_warnings = warnings;
    }

    void Collection::setRecursiveFlatMap (bool recursive) {
        this->recursive_flat_map = recursive;
    }

    Collection Collection::operator= (const Collection &collection) {
        this->workers_num = collection.workers_num;
        this->operation_queue = std::list<OperationData>(collection.operation_queue);
        this->data1 = std::map<RawData, RawData>(collection.data1);
        this->data2 = std::map<RawData, RawData>(collection.data2);
        if(collection.curr_data == &collection.data1) {
            this->curr_data = &this->data1;
            this->last_data = &this->data2;
        } else {
            this->curr_data = &this->data2;
            this->last_data = &this->data1;
        }
        this->element_type = collection.element_type;
        return *this;
    }

    Collection Collection::operator= (Collection &&collection) {
        this->workers_num = collection.workers_num;
        this->operation_queue = std::move(collection.operation_queue);
        this->data1 = std::move(collection.data1);
        this->data2 = std::move(collection.data2);
        if(collection.curr_data == &collection.data1) {
            this->curr_data = &this->data1;
            this->last_data = &this->data2;
        } else {
            this->curr_data = &this->data2;
            this->last_data = &this->data1;
        }
        this->element_type = collection.element_type;
        collection.reset();
        return *this;
    }
}
