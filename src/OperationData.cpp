#include <cstring>

#include <OperationData.hpp>

namespace lmr {
    OperationData::OperationData (operationType operation): operation_type(operation) {
        this->nullify();
    }

    OperationData::OperationData (std::string load_arg) {
        this->nullify();
        this->operation_type = operationType::LoadFromFile;
        this->load_arg.push_back(load_arg);
    }

    OperationData::OperationData (std::vector<std::string> &load_arg) {
        this->nullify();
        this->operation_type = operationType::LoadFromFile;
        this->load_arg = load_arg;
    }

    OperationData::OperationData (std::map<RawData, RawData>* join_arg) {
        this->nullify();
        this->operation_type = operationType::Join;
        this->join_arg = join_arg; // we should avoid copying a large amount of data
    }

    OperationData::OperationData (mapCallback map_cb, bool is_flat_map) {
        this->nullify();
        if(is_flat_map) {
            this->operation_type = operationType::FlatMap;
        } else {
            this->operation_type = operationType::Map;
        }
        this->map_cb = map_cb;
    }

    OperationData::OperationData (reduceCallback reduce_cb) {
        this->nullify();
        this->operation_type = operationType::Reduce;
        this->reduce_cb = reduce_cb;
    }

    OperationData::OperationData (filterCallback filter_cb) {
        this->nullify();
        this->operation_type = operationType::Filter;
        this->filter_cb = filter_cb;
    }

    OperationData::OperationData (const OperationData &op_data) {
        this->operation_type = op_data.operation_type;
        this->load_arg = op_data.load_arg;
        if(op_data.join_arg) {
            this->join_arg = new std::map<RawData, RawData>(*(op_data.join_arg));
        } else {
            this->join_arg = NULL;
        }
        this->map_cb = op_data.map_cb;
        this->reduce_cb = op_data.reduce_cb;
        this->filter_cb = op_data.filter_cb;
    }

    OperationData::OperationData (OperationData &&op_data): load_arg(std::move(op_data.load_arg)) {
        this->operation_type = op_data.operation_type;
        this->join_arg = op_data.join_arg;
        this->map_cb = op_data.map_cb;
        this->reduce_cb = op_data.reduce_cb;
        this->filter_cb = op_data.filter_cb;
        op_data.nullify();
        op_data.operation_type = operationType::None; /* do it after nullify() to not confuse the reader whether operation_type is touched in nullify() or not */
    }

    OperationData::~OperationData () {
        this->reset();
    }

    void OperationData::nullify () {
        this->load_arg.clear();
        this->join_arg = NULL;
        this->map_cb = NULL;
        this->reduce_cb = NULL;
        this->filter_cb = NULL;
    }

    void OperationData::reset () {
        // only map needs to be deleted, other cases will be handled by destructors etc.
        if(this->operation_type == operationType::Join && this->join_arg) {
            delete this->join_arg;
            this->join_arg = NULL;
        }
        this->nullify();
        this->operation_type = operationType::None; /* do it after nullify() to not confuse the reader whether operation_type is touched in nullify() or not */
    }

    OperationData& OperationData::operator= (const OperationData &op_data) {
        this->reset();
        this->operation_type = op_data.operation_type;
        this->load_arg = op_data.load_arg;
        if(op_data.operation_type == operationType::Join && op_data.join_arg) {
            this->join_arg = new std::map<RawData, RawData>(*(op_data.join_arg));
        }
        this->map_cb = op_data.map_cb;
        this->reduce_cb = op_data.reduce_cb;
        this->filter_cb = op_data.filter_cb;
        return *this;
    }

    OperationData& OperationData::operator= (OperationData &&op_data) {
        this->reset();
        this->operation_type = op_data.operation_type;
        this->load_arg = std::move(op_data.load_arg);
        this->join_arg = op_data.join_arg;
        this->map_cb = op_data.map_cb;
        this->reduce_cb = op_data.reduce_cb;
        this->filter_cb = op_data.filter_cb;
        op_data.nullify();
        op_data.operation_type = operationType::None; /* do it after nullify() to not confuse the reader whether operation_type is touched in nullify() or not */
        return *this;
    }
}
