#include <iostream>
#include <map>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cerrno>

#include <LocalMapReduce>

/* checks if randomized point is within the circle */
static lmr::RawData check_point (const lmr::RawData &number) {
    float x = drand48() * 2.0f - 1.0f;
    float y = drand48() * 2.0f - 1.0f;
    lmr::pair p;

    p.first = 'a';
    p.second = (x * x + y * y < 1.0f) ? 1U : 0U; /* square(1) = 1 */
    return lmr::RawData(p);
} /* check_point */

static lmr::RawData add (const lmr::RawData &key, const lmr::RawData &count1, const lmr::RawData &count2) {
    unsigned int c1 = count1.get<unsigned int>();
    unsigned int c2 = count2.get<unsigned int>();
    return lmr::RawData(c1 + c2);
} /* add */

int main (void) {
    unsigned int n = 100000 * 2;
    lmr::Collection collection;
    lmr::map input, *output;
    time_t t;
    
    if(time(&t) < 0) {
        std::cerr << "ERROR: Cannot get current time: " << strerror(errno) << std::endl;
        return 1;
    }
    srand(t);
    for(unsigned int i=0; i<n; i++) {
        input[i] = i;
    }
    collection.loadFromMap(input).map(check_point).reduce(add);
    output = collection.collect();
    /* there should be only one entry but using for loop is a nice debug */
    for(lmr::pair p: *output) {
        std::cout << "Pi is roughly " << ((float)p.second.get<unsigned int>() * 4.0f) / n << std::endl;
    }
    delete output;
    return 0;
} /* main */

