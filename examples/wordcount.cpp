#define _XOPEN_SOURCE /* strtok_r */

#include <iostream>
#include <string>
#include <map>
#include <cstdlib>
#include <cstring>

#include <LocalMapReduce>

static lmr::RawData split_lines (const lmr::RawData &raw_line) {
    char *line_c, *token, *savestr;
    std::string line = raw_line.get<std::string>();
    lmr::vector words;

    line_c = strdup(line.c_str());
    if(line_c) {
        token = strtok_r(line_c, " \t", &savestr); /* strtok_r because LMR may use many threads */
        while(token) {
            words.push_back(lmr::RawData(std::string(token)));
            token = strtok_r(NULL, " \t", &savestr);
        }
        free(line_c);
    } else {
        std::cerr << "ERROR: Cannot duplicate the string" << std::endl;
    }
    return lmr::RawData(words);
} /* split_lines */

static lmr::RawData make_counter (const lmr::RawData &word) {
    lmr::pair p;

    p.first = word.get<std::string>();
    p.second = 1U; /* we want to use unsigned int */
    return lmr::RawData(p);
} /* make_counter */

static lmr::RawData sum_up_words (const lmr::RawData &key, const lmr::RawData &value1, const lmr::RawData &value2) {
    unsigned int new_value = value1.get<unsigned int>() + value2.get<unsigned int>();
    return lmr::RawData(new_value);
} /* sum_up_words */

int main (int argc, char **argv) {
    lmr::Collection collection;
    lmr::map *output;

    if(argc < 2) {
        std::cout << "Usage: wordcount FILE1, [FILE2, ...]" << std::endl;
        std::cerr << "ERROR: Wrong number of arguments" << std::endl;
        return 1;
    }
    for(int i=1 ; i<argc ; i++)
        collection.loadFromFile(argv[i]);
    collection.flatMap(split_lines).map(make_counter).reduce(sum_up_words);
    output = collection.collect();
    for(lmr::pair p: *output) {
        std::cout << p.first.get<std::string>() << ": " << p.second.get<unsigned int>() << std::endl;
    }
    delete output;
    return 0;
} /* main */

