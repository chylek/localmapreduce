CC=g++
FLAGS=-mfpmath=sse -msse -msse2 -pedantic -g -Wextra -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization -Wendif-labels -Wfloat-equal -Wformat=2 -Wformat-nonliteral -Winline -Wmissing-declarations -Wno-unused-parameter -Wpointer-arith -Wstack-protector -Wswitch -Wundef -Wwrite-strings -O0 -std=c++14 -pthread
INCLUDE=-Iinclude
SRC_DIR=src
OBJ_DIR=obj

all: lib examples tests

operation-data:
	$(CC) $(FLAGS) -c $(SRC_DIR)/OperationData.cpp -o $(OBJ_DIR)/OperationData.o $(INCLUDE)

dataqueue:
	$(CC) $(FLAGS) -c $(SRC_DIR)/DataQueue.cpp -o $(OBJ_DIR)/DataQueue.o $(INCLUDE)

lmr:
	$(CC) $(FLAGS) -c $(SRC_DIR)/LocalMapReduce.cpp -o $(OBJ_DIR)/LocalMapReduce.o $(INCLUDE)

lib: lmr operation-data dataqueue
	ar rcs liblmr.a $(OBJ_DIR)/LocalMapReduce.o $(OBJ_DIR)/OperationData.o $(OBJ_DIR)/DataQueue.o

examples: lib
	make -C examples

tests: lib
	make -C tests

old:
	make -C old

clean:
	rm -f $(OBJ_DIR)/* liblmr.a
	make -C examples clean
	make -C tests clean
	make -C old clean

.PHONY: all examples tests old clean
