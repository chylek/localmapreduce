#include <cstdio>
#include <cstring>

int main (void) {
    char line[4096], *token; // WARNING: possible buffer overflow !!

    while(scanf("%s", line) == 1) {
        token = strtok(line, " \t");
        while(token) {
            printf("%s 1\n", token);
            token = strtok(NULL, " \t");
        }
    }
    return 0;
}
