#include <cstdio>
#include <cstring>

int main (void) {
    unsigned int c1 = 0, c2;
    char word1[1024], word2[1024];

    scanf("%s %u", word1, &c1);
    while(scanf("%s %u", word2, &c2) == 2) {
        if(strcmp(word1, word2)) {
            printf("%s: %u\n", word1, c1);
            strcpy(word1, word2);
            c1 = c2;
        } else {
            c1 += c2;
        }
    }
    if(c1) {
        printf("%s: %u\n", word1, c1);
    }
    return 0;
}

