#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cinttypes>
#include <algorithm>
#include <unistd.h>
#include <sys/wait.h>
#include "cmdline.h"

struct gengetopt_args_info args;

static ssize_t my_getline (char **line, int fd) {
    ssize_t bytes = 0, length = 100;

    *line = (char*)malloc(length + 1);
    if(!*line) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return -1;
    }
    while(1) {
        switch(read(fd, *line + bytes, 1)) {
            case -1:
                fprintf(stderr, "ERROR: Cannot read the character: %s\n", strerror(errno));
                free(*line);
                return -1;
            case 0: /* EOF */
                (*line)[bytes] = '\0';
                return bytes;
            case 1:
                bytes++;
                if((*line)[bytes-1] == '\n') {
                    (*line)[bytes] = '\0';
                    return bytes;
                }
                if(bytes == length) {
                    void *ptr = realloc(*line, length + 101);
                    if(!ptr) {
                        fprintf(stderr, "ERROR: Out of memory\n");
                        free(*line);
                        return -1;
                    }
                    *line = (char*)ptr;
                    length += 100;
                }
                break;
            default:
                fprintf(stderr, "ERROR: Unexpected result\n");
                free(*line);
                return -1;
        }
    }
}

static int get_index (const char *key, unsigned int length, int modulo) {
    int n = 0;

    for(unsigned int i=0; i<length; i++)
        n += (unsigned char)key[i];
    return n % modulo;
}

static int alloc_pipes (int ***ptr, int size) {
    int i = 0, ret = -1;

    *ptr = (int**)calloc(size, sizeof(int*));
    if(!*ptr) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ret;
    }
    for(; i<size; i++) {
        (*ptr)[i] = (int*)malloc(2 * sizeof(int));
        if(!(*ptr)[i]) {
            fprintf(stderr, "ERROR: Out of memory\n");
            goto alloc_pipes_err;
        }
        (*ptr)[i][0] = (*ptr)[i][1] = -1;
    }

    ret = 0;
alloc_pipes_err:
    if(ret) {
        for(i--; i>=0; i--)
            free((*ptr)[i]);
        free(*ptr);
        *ptr = NULL;
    }
    return ret;
}

static int create_pipes (int ***ptr, int size) {
    int i = 0, ret = -1;

    if(alloc_pipes(ptr, size))
        return ret;
    for(; i<size; i++)
        if(pipe((*ptr)[i])) {
            fprintf(stderr, "ERROR: Cannot create new pipe: %s\n", strerror(errno));
            goto create_pipes_err;
        }

    ret = 0;
create_pipes_err:
    if(ret)
        for(i--; i>=0; i--) {
            close((*ptr)[i][0]);
            close((*ptr)[i][1]);
        }
    return ret;
}

static void pipes_free (int **ptr, int size) {
    if(ptr) {
        for(int i=0; i<size; i++)
            if(ptr[i]) {
                if(ptr[i][0] >= 0)
                    close(ptr[i][0]);
                if(ptr[i][1] >= 0)
                    close(ptr[i][1]);
                free(ptr[i]);
            }
        free(ptr);
    }
}

static int run_mappers (int **mappers, int **redirectors, int mappers_size, int **sorters, pid_t *reducers, int reducers_size) {
    int ret, fdin, fdout;

    for(int i=0; i<mappers_size; i++)
        switch(fork()) {
            case -1:
                ret = errno;
                fprintf(stderr, "ERROR: fork() failed: %s\n", strerror(errno));
                goto run_mappers_err;
            case 0: /* child process */
                fdin = mappers[i][0];
                fdout = redirectors[i][1];
                mappers[i][0] = -1;
                redirectors[i][1] = -1;
                free(reducers);
                pipes_free(mappers, mappers_size);
                pipes_free(redirectors, mappers_size);
                pipes_free(sorters, reducers_size);
                if(dup2(fdin, STDIN_FILENO) == -1) { /* set read end as stdin */
                    ret = errno;
                    fprintf(stderr, "ERROR: Cannot redirect stdin: %s\n", strerror(errno));
                    close(fdin);
                    close(fdout);
                    exit(ret);
                }
                if(dup2(fdout, STDOUT_FILENO) == -1) { /* set write end as stdout */
                    ret = errno;
                    fprintf(stderr, "ERROR: Cannot redirect stdout: %s\n", strerror(errno));
                    close(fdout);
                    exit(ret);
                }
                execl(args.inputs[0], args.inputs[0], NULL);
                ret = errno;
                fprintf(stderr, "ERROR: execv() failed: %s\n", strerror(errno));
                exit(ret);
            default: /* parent process */
                break;
        }

    ret = 0;
run_mappers_err:
    return ret;
}

static int run_redirectors (int **mappers, int **redirectors, int mappers_size, int **sorters, pid_t *reducers, int reducers_size) {
    int ret, fd, sorter;
    unsigned int length;
    ssize_t bytes;
    char *line = NULL;

    for(int i=0; i<mappers_size; i++)
        switch(fork()) {
            case -1:
                ret = errno;
                fprintf(stderr, "[Redirector] ERROR: fork() failed: %s\n", strerror(errno));
                goto run_redirectors_err;
            case 0: /* child process */
                fd = redirectors[i][0];
                redirectors[i][0] = -1;
                free(reducers);
                pipes_free(mappers, mappers_size);
                pipes_free(redirectors, mappers_size);
                cmdline_parser_free(&args);
                while((bytes = my_getline(&line, fd)) >= 0) {
                    if(!bytes) {
                        free(line);
                        line = NULL;
                        break;
                    }
                    length = 0;
                    while(length < bytes && line[length] > 32)
                        length++;
                    if(length == bytes) {
                        fprintf(stderr, "[Redirector] ERROR: White character not found in '%s'\n", line);
                        free(line);
                        close(fd);
                        pipes_free(sorters, reducers_size);
                        exit(EINVAL);
                    }
                    sorter = get_index(line, length, reducers_size);
                    if(write(sorters[sorter][1], line, bytes) != bytes) {
                        ret = errno;
                        fprintf(stderr, "[Redirector] ERROR: Wrote less then expected: %s\n", strerror(errno));
                        close(fd);
                        pipes_free(sorters, reducers_size);
                        exit(ret);
                    }
                    free(line);
                    line = NULL;
                }
                close(fd);
                pipes_free(sorters, reducers_size);
                if(bytes < 0) {
                    ret = errno;
                    fprintf(stderr, "[Redirector] ERROR: Cannot read new line: %s\n", strerror(errno));
                    exit(ret);
                }
                exit(0);
            default: /* parent process */
                break;
        }

    ret = 0;
run_redirectors_err:
    return ret;
}

static bool string_cmp (char *str1, char *str2) {
    if(strcmp(str1, str2) < 0)
        return true;
    return false;
}

static int run_sorters (int **mappers, int **redirectors, int mappers_size, int **sorters, pid_t **reducers, int reducers_size) {
    int ret, status, fd, p[2];
    size_t nlines = 0, maxlines = 100;
    ssize_t bytes;
    pid_t pid = 0;
    char **lines = NULL;

    *reducers = (pid_t*)malloc(reducers_size * sizeof(pid_t));
    if(!*reducers) {
        fprintf(stderr, "[Sorter] ERROR: Out of memory\n");
        return ENOMEM;
    }
    for(int i=0; i<reducers_size; i++)
        switch(((*reducers)[i] = fork())) {
            case -1:
                ret = errno;
                fprintf(stderr, "[Sorter] ERROR: fork() failed: %s\n", strerror(errno));
                goto run_sorters_err;
            case 0: /* child process */
                fd = sorters[i][0];
                sorters[i][0] = -1;
                pipes_free(mappers, mappers_size);
                pipes_free(redirectors, mappers_size);
                pipes_free(sorters, reducers_size);
                if(pipe(p)) {
                    ret = errno;
                    fprintf(stderr, "[Sorter] ERROR: Cannot create new pipe: %s\n", strerror(errno));
                    close(fd);
                    cmdline_parser_free(&args);
                    exit(ret);
                }
                switch((pid = fork())) {
                    case -1:
                        ret = errno;
                        fprintf(stderr, "[Sorter] ERROR: fork() failed: %s\n", strerror(errno));
                        cmdline_parser_free(&args);
                        goto sorter_err;
                    case 0: /* child process */
                        close(p[1]);
                        if(dup2(p[0], STDIN_FILENO) == -1) { /* set read end as stdin */
                            ret = errno;
                            fprintf(stderr, "[Sorter] ERROR: Cannot redirect stdin: %s\n", strerror(errno));
                            goto sorter_err;
                        }
                        execl(args.inputs[1], args.inputs[2], NULL);
                        ret = errno;
                        fprintf(stderr, "[Sorter] ERROR: execv() failed: %s\n", strerror(errno));
                        exit(errno);
                    default: /* parent process */
                        break;
                }
                cmdline_parser_free(&args);
                lines = (char**)calloc(maxlines, sizeof(char*));
                if(!lines) {
                    fprintf(stderr, "[Sorter] ERROR: Out of memory\n");
                    ret = ENOMEM;
                    goto sorter_err;
                }
                while((bytes = my_getline(lines + nlines, fd)) >= 0) {
                    nlines++;
                    if(!bytes)
                        break;
                    if(nlines == maxlines) {
                        char **ptr = (char**)realloc(lines, (maxlines+100) * sizeof(char*));
                        if(!ptr) {
                            fprintf(stderr, "[Sorter] ERROR: Out of memory\n");
                            ret = ENOMEM;
                            goto sorter_err;
                        }
                        maxlines += 100;
                        lines = ptr;
                        memset(lines + nlines, 0, 100);
                    }
                }
                if(bytes < 0) {
                    ret = errno;
                    fprintf(stderr, "[Sorter] ERROR: Cannot read new line: %s\n", strerror(errno));
                    goto sorter_err;
                }
                if(nlines)
                    std::sort(lines, lines+nlines, string_cmp);
                for(size_t j=0; j<nlines; j++)
                    if((bytes = write(p[1], lines[j], strlen(lines[j]))) != (ssize_t)strlen(lines[j])) {
                        if(bytes < 0) {
                            ret = errno;
                            fprintf(stderr, "[Sorter] ERROR: Cannot write to the reducer: %s\n", strerror(errno));
                            goto sorter_err;
                        }
                        fprintf(stderr, "[Sorter] ERROR: Wrote less (%ld < %ld) than expected: %s\n", (long int)bytes, strlen(lines[j]), strerror(errno));
                        ret = EINVAL;
                        goto sorter_err;
                    }

                ret = 0;
            sorter_err:
                if(lines) {
                    for(size_t j=0; j<nlines; j++)
                        free(lines[j]);
                    free(lines);
                }
                close(fd);
                close(p[0]);
                close(p[1]);
                if(pid > 0)
                    while(1) {
                        if(waitpid(pid, &status, 0) < 0) {
                            ret = errno;
                            fprintf(stderr, "[Sorter] ERROR: Cannot get reducer status: %s\n", strerror(errno));
                            goto sorter_err;
                        }
                        if(WIFEXITED(status) || WIFSIGNALED(status))
                            break;
                    }
                exit(ret);
            default: /* parent process */
                break;
        }

    ret = 0;
run_sorters_err:
    return ret;
}

int main (int argc, char **argv) {
    int ret, status, mapper = 0, **mappers = NULL, **redirectors = NULL, **sorters = NULL;
    ssize_t bytes;
    size_t n = 0;
    pid_t *reducers = NULL;
    char *line = NULL;
    FILE *file = NULL;

    if(cmdline_parser(argc, argv, &args)) {
        cmdline_parser_print_help();
        fprintf(stderr, "\nERROR: Incorect usage\n");
        return EINVAL;
    }
    if(args.inputs_num != 3) {
        cmdline_parser_print_help();
        fprintf(stderr, "\nERROR: Wrong number of arguments\n");
        ret = EINVAL;
        goto main_err;
    }
    if(args.mappers_arg <= 0 || args.reducers_arg <= 0) {
        cmdline_parser_print_help();
        fprintf(stderr, "\nERROR: Please set positive nubmer of workers\n");
        ret = EINVAL;
        goto main_err;
    }
    if(create_pipes(&mappers, args.mappers_arg)) {
        ret = ENOMEM;
        goto main_err;
    }
    if(create_pipes(&redirectors, args.mappers_arg)) {
        ret = ENOMEM;
        goto main_err;
    }
    if(create_pipes(&sorters, args.reducers_arg)) {
        ret = ENOMEM;
        goto main_err;
    }
    if((ret = run_sorters(mappers, redirectors, args.mappers_arg, sorters, &reducers, args.reducers_arg)))
        goto main_err;
    if((ret = run_redirectors(mappers, redirectors, args.mappers_arg, sorters, reducers, args.reducers_arg)))
        goto main_err;
    if((ret = run_mappers(mappers, redirectors, args.mappers_arg, sorters, reducers, args.reducers_arg)))
        goto main_err;
    file = fopen(args.inputs[2], "r");
    if(!file) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", args.inputs[2], strerror(errno));
        goto main_err;
    }
    while((bytes = getline(&line, &n, file)) >= 0) {
        if(write(mappers[mapper][1], line, bytes) != bytes) {
            ret = errno;
            fprintf(stderr, "ERROR: Wrote less then expected: %s\n", strerror(errno));
            goto main_err;
        }
        mapper++;
        if(mapper == args.mappers_arg)
            mapper = 0;
        free(line);
        line = NULL;
        n = 0;
    }
    free(line);
    if(bytes < 0 && errno) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot read new line: %s\n", strerror(errno));
        goto main_err;
    }

    ret = 0;
main_err:
    pipes_free(mappers, args.mappers_arg);
    pipes_free(redirectors, args.mappers_arg);
    pipes_free(sorters, args.reducers_arg);
    if(file)
        fclose(file);
    if(!ret) {
        for(int i=0; i<args.reducers_arg;) {
            if(waitpid(reducers[i], &status, 0) < 0) {
                ret = errno;
                fprintf(stderr, "ERROR: Cannot get children status: %s\n", strerror(errno));
                goto main_err;
            }
            if(WIFEXITED(status) || WIFSIGNALED(status))
                i++;
        }
    }
    free(reducers);
    cmdline_parser_free(&args);
    return ret;
}

