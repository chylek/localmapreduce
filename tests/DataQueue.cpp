#include <iostream>
#include <utility>

#include <DataQueue.hpp>

/* TODO:
 * - concurrency
 */

int main (void) {
    lmr::RawData rd(0);
    lmr::DataQueue q1, q2;

    /* push(), empty(), size() */
    std::cout << "DataQueue empty: " << (q1.empty() ? "true" : "false") << " (size: " << q1.size() << ")" << std::endl;
    std::cout << "> Using .push()" << std::endl;
    for(int i=0; i<10; i++) {
        q1.push(lmr::RawData(i));
    }
    std::cout << "DataQueue empty: " << (q1.empty() ? "true" : "false") << " (size: " << q1.size() << ")" << std::endl;

    /* front() */
    std::cout << std::endl;
    if(!q1.front(rd)) {
        std::cerr << "ERROR: Cannot read first value from data queue" << std::endl;
        return 1;
    }
    std::cout << "Front: " << rd.get<int>() << std::endl;

    /* back() */
    if(!q1.back(rd)) {
        std::cerr << "ERROR: Cannot read last value from data queue" << std::endl;
        return 1;
    }
    std::cout << "Back: " << rd.get<int>() << std::endl;

    /* pop() */
    if(!q1.pop(rd)) {
        std::cerr << "ERROR: Cannot get first value from data queue" << std::endl;
        return 1;
    }
    std::cout << "Pop: " << rd.get<int>() << " (size: " << q1.size() << ")" << std::endl;

    /* getEOT(), setEOT() */
    std::cout << std::endl;
    std::cout << "End of transmission: " << (q1.getEOT() ? "true" : "false") << std::endl;
    std::cout << "> Using .setEOT(true)" << std::endl;
    q1.setEOT(true);
    std::cout << "End of transmission: " << (q1.getEOT() ? "true" : "false") << std::endl;
    std::cout << "> Using .setEOT(false)" << std::endl;
    q1.setEOT(false);
    std::cout << "End of transmission: " << (q1.getEOT() ? "true" : "false") << std::endl;

    /* operator= */
    std::cout << std::endl;
    std::cout << q1.size() << " " << q2.size() << std::endl;
    std::cout << "> Using operator=" << std::endl;
    q2 = q1;
    std::cout << q1.size() << " " << q2.size() << std::endl;

    /* move operator= */
    std::cout << std::endl;
    std::cout << q1.size() << " " << q2.size() << std::endl;
    std::cout << "> Using move operator=" << std::endl;
    q2 = std::move(q1);
    std::cout << q1.size() << " " << q2.size() << std::endl;

    /* move constructor */
    std::cout << std::endl;
    std::cout << "Size: " << q2.size() << " X"  << std::endl;
    std::cout << "> Using move constructor" << std::endl;
    lmr::DataQueue q_move(std::move(q2));
    std::cout << "Size: " << q2.size() << " " << q_move.size() << std::endl;

    /* clear() */
    std::cout << std::endl;
    std::cout << "Size: " << q_move.size() << std::endl;
    std::cout << "> Using .clear()" << std::endl;
    q_move.clear();
    std::cout << "Size: " << q_move.size() << std::endl;

    return 0;
}

