#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <LocalMapReduce>

int main (void) {
    lmr::vector input_v;
    lmr::map input_m, *output;
    lmr::Collection col, colw(10), col2(col), colw2(std::move(colw));

    colw = colw2;
    for(int i=0; i<10; i++) {
        input_v.push_back(i);
        input_m[i] = i;
    }

    // move operator is used for every temporary variable
    // load data from file
    col = lmr::Collection();
    col.loadFromFile("input_file.txt");
    std::cout << "Size: " << col.size() << std::endl;
    output = col.collect();
    delete output;

    // load data from vector + assignment operator
    col2 = lmr::Collection();
    col2.loadFromVector(input_v);
    col = col2;
    std::cout << "Size: " << col.size() << std::endl;
    output = col.collect();
    delete output;

    // load data from map + move operator
    col2 = lmr::Collection();
    col2.loadFromMap(input_m);
    col = std::move(col2);
    std::cout << "Size: " << col.size() << std::endl;
    output = col.collect();
    delete output;

    // operations: map, flatMap, reduce, filter, join
    // will be tested using examples

    return 0;
}

