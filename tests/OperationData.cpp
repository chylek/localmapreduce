#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <OperationData.hpp>

static lmr::RawData map (const lmr::RawData &data) {
    return lmr::RawData(data);
}

static lmr::RawData reduce (const lmr::RawData &key, const lmr::RawData &value1, const lmr::RawData &value2) {
    return lmr::RawData(value1);
}

static bool filter (const lmr::RawData &data) {
    return true;
}

int main (void) {
    std::vector<std::string> files = {"file1", "file2", "file3", "file4", "file5"};
    std::map<lmr::RawData, lmr::RawData> join_arg;
    lmr::OperationData opd_none,
                       opd_loadf("file"),
                       opd_loadv(files),
                       opd_map(map),
                       opd_flatmap(map, true),
                       opd_reduce(reduce),
                       opd_filter(filter),
                       opd_join(&join_arg),
                       opd_collect(lmr::operationType::Collect),
                       opd_none_type(lmr::operationType::None),
                       opd_load_type(lmr::operationType::LoadFromFile),
                       opd_map_type(lmr::operationType::Map),
                       opd_flatmap_type(lmr::operationType::FlatMap),
                       opd_reduce_type(lmr::operationType::Reduce),
                       opd_filter_type(lmr::operationType::Filter),
                       opd_join_type(lmr::operationType::Join),
                       opd_collect_type(lmr::operationType::Collect),
                       opd_loadv_copy(opd_loadv),
                       opd_move(std::move(opd_loadv_copy)),
                       opd_none2, opd_loadf2, opd_loadv2, opd_map2, opd_flatmap2, opd_reduce2, opd_filter2, opd_join2, opd_collect2, opd_none_type2, opd_load_type2, opd_map_type2, opd_flatmap_type2, opd_reduce_type2, opd_filter_type2, opd_join_type2, opd_collect_type2, opd_loadv_copy2, opd_move2;

    opd_loadv_copy = std::move(opd_move);
    opd_move.reset();
    opd_none2 = opd_none;
    opd_loadf2 = opd_loadf;
    opd_loadv2 = opd_loadv;
    opd_map2 = opd_map;
    opd_flatmap2 = opd_flatmap;
    opd_reduce2 = opd_reduce;
    opd_filter2 = opd_filter;
    opd_join2 = opd_join;
    opd_collect2 = opd_collect;
    opd_none_type2 = opd_none_type;
    opd_load_type2 = opd_load_type;
    opd_map_type2 = opd_map_type;
    opd_flatmap_type2 = opd_flatmap_type;
    opd_reduce_type2 = opd_reduce_type;
    opd_filter_type2 = opd_filter_type;
    opd_join_type2 = opd_join_type;
    opd_collect_type2 = opd_collect_type;

    opd_none = std::move(opd_loadf2);
    opd_loadf = std::move(opd_loadv2);
    opd_loadv = std::move(opd_map2);
    opd_map = std::move(opd_flatmap2);
    opd_flatmap = std::move(opd_reduce2);
    opd_reduce = std::move(opd_filter2);
    opd_filter = std::move(opd_join2);
    opd_join = std::move(opd_collect2);
    opd_collect = std::move(opd_loadv_copy);

    return 0;
}

