#include <iostream>
#include <utility>

#include <RawData.hpp>

struct comparision {
    std::string name;
    lmr::RawDataCmp fun;
};

static bool cmp (const lmr::RawData &data1, const lmr::RawData &data2) {
    /* opposite comparision just to have something different than default one */
    return data1.get<int>() >= data2.get<int>();
} /* cmp */

int values[][2] = {
    {0, 0},
    {0, 1},
    {1, 0},
    {0, -1},
    {-1, 0},
    {-2, 3}
};

comparision tests[] = {
    " < ", [](const lmr::RawData &data1, const lmr::RawData &data2) -> bool { return data1 < data2; },
    " <= ", [](const lmr::RawData &data1, const lmr::RawData &data2) -> bool { return data1 <= data2; },
    " > ", [](const lmr::RawData &data1, const lmr::RawData &data2) -> bool { return data1 > data2; },
    " >= ", [](const lmr::RawData &data1, const lmr::RawData &data2) -> bool { return data1 >= data2; },
    " == ", [](const lmr::RawData &data1, const lmr::RawData &data2) -> bool { return data1 == data2; },
};

int main (void) {
    /* constructors */
    lmr::RawData rd1(2), rd2(1, cmp), rd_copy(rd1), rd_move(std::move(rd2));

    /* get() */
    std::cout << rd1.get<int>() << ", " << rd_move.get<int>() << std::endl;

    /* operator= */
    rd1 = lmr::RawData(1);
    rd2 = 2;

    /* operator= && */
    rd_move = std::move(rd2);
    rd2 = 2; /* rd2 is cleared after using it in move operator (here: no needed, int is not an object) */

    /* comparisions */
    for(int *val: values) {
        rd1 = val[0];
        rd2 = val[1];
        for(comparision cmp: tests) {
            std::cout << rd1.get<int>() << cmp.name << rd2.get<int>() << " : " << (cmp.fun(rd1, rd2) ? "true" : "false") << std::endl;
        }
    }
    for(int *val: values) {
        rd1 = val[0];
        for(comparision cmp: tests) {
            std::cout << rd1.get<int>() << cmp.name << val[1] << " : " << (cmp.fun(rd1, val[1]) ? "true" : "false") << " (using int)"<< std::endl;
        }
    }
    for(int *val: values) {
        rd1 = lmr::RawData(val[0], cmp);
        rd2 = val[1]; /* comparision function only from rd1 will be used */
        for(comparision cmp: tests) {
            std::cout << rd1.get<int>() << cmp.name << rd2.get<int>() << " : " << (cmp.fun(rd1, rd2) ? "true" : "false") << " (custom comparator used)" << std::endl;
        }
    }

    return 0;
} /* main */

