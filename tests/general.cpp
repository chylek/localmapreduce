#include <cstdio>
#include <map>
#include <vector>
#include <string.h>

#include <LocalMapReduce>

#define LMR_RECURSIVE_FLAT_MAP false

//def splitLine(rawdata):
//    pos = str.find(" ")
//    i = 0
//    v  = []
//    while pos != -1:
//        v.append(str[i:pos])
//        i = pos+1
//        pos = str.find(" ", i)
//
//    v.append(str[i:len(str)])
//    return v;

int main (void) {
    lmr::Collection myCollection;
    lmr::Collection cc;
    myCollection.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    cc = myCollection;                                                                                                                                 //conf = SparkConf().setAppName(appName).setMaster(master)
                                                                                                                                                                                           //sc = SparkContext(conf=conf)
    myCollection.loadFromFile("test_file1.txt");                                                                                                                   //myCollection = sc.textFile("data.txt")    /
    myCollection.loadFromFile("test_file2.txt");
    myCollection.filter([](const lmr::RawData &rawdata)->bool{                                                                                                                    //.filter(lambda rawdata: return rawdata[0]!='s')   /
        return rawdata.get<std::string>()[0]!='s';
    }).flatMap([](const lmr::RawData &rawdata)->lmr::RawData{                                                                                               //.flatMap(splitLine)   /
        std::string str = rawdata.get<std::string>();
        size_t pos, i=0;
        lmr::RawData v  = lmr::vector(); // because it's cheaper to return
        while((pos = str.find(" ",i)) != std::string::npos){
            v.get<lmr::vector>().push_back(str.substr(i, pos-i));
            i = pos+1;
        }
        v.get<lmr::vector>().push_back(str.substr(i, str.length()-i));
        return v;
    }).map([](const lmr::RawData &rawdata)->lmr::RawData{                                                                                                      //.map(lambda rawdata: return (rawdata, 1)) /
        lmr::pair kv(rawdata, 1);
        return kv;
    }).reduce([](const lmr::RawData &key, const lmr::RawData &a, const lmr::RawData &b)->lmr::RawData{                                                                                  //.reduceByKey(lambda a, b: return a+b)    /
        return (a.get<int>() + b.get<int>());
    });
    lmr::map *output = myCollection.collect();                                                               //output = myCollection.collect()

    std::cout << "\noutput:" << std::endl;                                                                                                                          //print "\noutput:"
    for(lmr::pair record : *output)                                                                                     //for i in output:
        std::cout<< record.first.get<std::string>() << "\t" << record.second.get<int>() << std::endl;                           //    print i, out[m]

    myCollection.map([](const lmr::RawData &rawdata)->lmr::RawData{                                                                                   //myCollection = myCollection.map(lambda rawdata: return (rawdata[1],1)) /
        lmr::pair &kv = rawdata.get<lmr::pair>();
        kv.first = kv.second;
        kv.second = 1;
        return rawdata;
    }).reduce([](const lmr::RawData &key, const lmr::RawData &a, const lmr::RawData &b)->lmr::RawData{                                                                                 //.reduceById(lambda a, b: return a+b)
        return a.get<int>() + b.get<int>();
    });

    delete output;
    output = myCollection.collect();                                                                                                                                   //output = myCollection.collect()

    std::cout << "\noutput2:" << std::endl;                                                                                                                       //print \noutput2:"
    for(lmr::pair record : *output)                                                                                    //for i in output:
        std::cout<< record.first.get<int>() << "\t" << record.second.get<int>() << std::endl;                                      //    print i, out[m]

    delete output;

    lmr::pair p;
    lmr::Collection lmpi, lmpf, lmp;
    lmr::RawData raw(5);

    lmpi.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    lmpf.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    lmp.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    lmr::map m1, m2;                                                                                                   //
    m1['a'] = 1;                                                                                                                                                                //
    m1['b'] = 2;                                                                                                                                                                //
    m1['c'] = 3;                                                                                                                                                                //
    m1['d'] = 4;                                                                                                                                                                //m1={'a':1, 'b':2, 'c':3, 'd':4}

    m2['a'] = 1.5f;                                                                                                                                                             //
    m2['c'] = 3.5f;                                                                                                                                                             //
    m2['e'] = 2.7f;                                                                                                                                                             //m2={'a':1.5, 'c':3.5, 'e':2.7}

    lmpi.loadFromMap(m1);                                                                                                                                              //lmpi = sc.parallelize(m1)
    lmpf.loadFromMap(m2);                                                                                                                                              //lmpf = sc.parallelize(m2)

    printf("\n\nJoining:\n");                                                                                                                                                 //print "\n\nJoining:\n"

    output = lmpi.collect();                                                                                                                                                //output = lmpi.collect()
    for(lmr::pair record : *output)                                                                                   //for i in output:
        std::cout<< record.first.get<char>() << "\t" << record.second.get<int>() << std::endl;                                  //    print i, out[m]
    delete output;

    printf("\nand:\n\n");                                                                                                                                                       //print "\nand:\n\n"

    output = lmpf.collect();                                                                                                                                                //output = lmpf.collect()
    for(lmr::pair record : *output)                                                                                   //for i in output:
        std::cout<< record.first.get<char>() << "\t" << record.second.get<float>() << std::endl;                               //    print i, out[m]
    delete output;

    lmp = lmpi.join(lmpf);                                                                                                                                                          //lmp = lmpi.join(lmpf)
    printf("\nResults in:\n\n");                                                                                                                                                      //print "\nResults in:\n\n"

    output = lmp.collect();                                                                                                                                                        //output = lmp.collect()
    for(lmr::pair record : *output){                                                                                           //for i in output:
        p = record.second.get<lmr::pair>();                                                                             //    p = out[m]
        std::cout<< record.first.get<char>() << "\t" << p.first.get<int>()<< "\t"<< p.second.get<float>() << std::endl;  //    print i, p[0], p[2]
    }
    delete output;

    printf("\nCollections sizes:\n");                                                                                                                                               //print "\nCollections sizes:\n"
    printf("%lu + %lu -> %lu\n", lmpi.size(), lmpf.size(), lmp.size());                                                                                      //print lmpi.size(), "+", lmpf.size(), "->", lmp->size()

    std::cout << "\n\nTesting map without reduce:\n"<<std::endl;

    lmr::Collection map_coll_test;
    lmr::map m3={{1 , 11} , {2 , 12} , {3 , 13}};
    map_coll_test.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    map_coll_test.loadFromMap(m3);

    map_coll_test.map([](const lmr::RawData &in)->lmr::RawData{
        return in;
    });
    output = map_coll_test.collect();
    for(lmr::pair record : *output){
        std::cout<< record.first.get<int>() << "\t" << record.second.get<int>()<< std::endl;
    }
    delete output;

    std::cout << "\n\ntesting map without reduce when data loaded from file:\n"<<std::endl;

    lmr::Collection map_coll_test2;
    map_coll_test2.setRecursiveFlatMap(LMR_RECURSIVE_FLAT_MAP);
    map_coll_test2.loadFromFile("data1.txt");
    map_coll_test2.map([](const lmr::RawData &rd)->lmr::RawData{
        int val;
        char key;
        std::string str = rd.get<std::string>();
        lmr::pair p;

        sscanf(str.c_str(), "%c %d", &key, &val);
        p.first = key;
        p.second = val;
        return p;
    });

    output = map_coll_test2.collect();
    for(lmr::pair record : *output){
        std::cout<< record.first.get<char>() << "\t" << record.second.get<int>()<< std::endl;
    }
    delete output;
    return 0;
}
