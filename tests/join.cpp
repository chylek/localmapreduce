#include <cstdio>
#include <string>
#include <map>
#include <cstring>

#include <LocalMapReduce>

static lmr::RawData str_to_int (const lmr::RawData &rd) {
    int val;
    char key;
    std::string str = rd.get<std::string>();
    lmr::pair p;

    sscanf(str.c_str(), "%c %d", &key, &val);
    p.first = key;
    p.second = val;
    return lmr::RawData(p);
}

static lmr::RawData str_to_float (const lmr::RawData &rd) {
    float val;
    char key;
    std::string str = rd.get<std::string>();
    lmr::pair p;

    sscanf(str.c_str(), "%c %f", &key, &val);
    p.first = key;
    p.second = val;
    return lmr::RawData(p);
}

static lmr::RawData reduce_int (const lmr::RawData &key, const lmr::RawData &rd1, const lmr::RawData &rd2) {
    lmr::pair   p1 = rd1.get<lmr::pair>(), p2 = rd2.get<lmr::pair>();
    return lmr::RawData(p1.second.get<int>() + p2.second.get<int>());
}

static lmr::RawData reduce_float (const lmr::RawData &key, const lmr::RawData &rd1, const lmr::RawData &rd2) {
    lmr::pair   p1 = rd1.get<lmr::pair>(), p2 = rd2.get<lmr::pair>();
    return lmr::RawData(p1.second.get<float>() + p2.second.get<float>());
}

int main () {
    lmr::pair p;
    lmr::map *m;
    lmr::Collection col1, col2;

    col1.loadFromFile("data1.txt").map(str_to_int).reduce(reduce_int);
    col2.loadFromFile("data2.txt").map(str_to_float).reduce(reduce_float).join(col1);
    m = col2.collect();
    for(lmr::map::iterator it=m->begin(); it!=m->end(); ++it) {
        p = it->second.get<lmr::pair>();
        printf("%c: %f %d\n", it->first.get<char>(), p.first.get<float>(), p.second.get<int>());
    }
    delete m;
    return 0;
}

